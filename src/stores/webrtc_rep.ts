import { get, writable } from "svelte/store";

export const UsableIceCandidate = writable([] as RTCIceCandidateInit[]);


const config: RTCConfiguration = {iceServers: [
    {
        urls: ['stun:stun.l.google.com:19302',
                "stun:stun1.l.google.com:19302",
                "stun:stun2.l.google.com:19302",
                "stun:stun3.l.google.com:19302",
                "stun:stun4.l.google.com:19302",
        ]
    }, 
    {
        urls: ["turn:59.127.60.115:3478"],
        username: "mycena",
        credential: "mycena"
    }
]}

// IceCandidate is offer to remote for addIcedidate.
let pc: RTCPeerConnection | undefined = undefined

export const initWebRTC = () => {
    if(pc !== undefined){
        return 0
    }
    pc = new RTCPeerConnection(config)

    pc.onicecandidate = (evt: RTCPeerConnectionIceEvent) => {
        if (evt.candidate){
            console.log(evt.candidate)
            const newCandidate = evt.candidate
            const currentCandidateList = get(UsableIceCandidate)
            currentCandidateList.push(newCandidate)
            UsableIceCandidate.set(currentCandidateList)
        }
    };
    

    pc.createOffer({
        offerToReceiveAudio: true, // 為了讓 iceGatheringState 進入gathering，原則上需要增加Track的內容，或是將左邊參數打開 (後來好像又不用了，暫時不查證)
        // offerToReceiveVideo: true, // https://stackoverflow.com/questions/61325035/no-ice-candidates-gathering-peerconnection-icegatheringstate-returns-complete
    }).then(function(offer) {
        return pc?.setLocalDescription(offer);
    }).then(function() {
        // wait for ICE gathering to complete
        // iceGatheringState = new -> gathering -> complete
        console.log("Waiting for iceGathering");
        return new Promise(function(resolve) {
            if (pc?.iceGatheringState === 'complete') {
                resolve(0);
            } else {
                // eslint-disable-next-line no-inner-declarations
                function checkState() {
                    if (pc?.iceGatheringState === 'complete') {
                        pc?.removeEventListener('icegatheringstatechange', checkState);
                        console.log("IceGathering complete");
                        resolve(0);
                    }
                }
                pc?.addEventListener('icegatheringstatechange', checkState);
            }
        });
    }).catch(function(e) {
        alert(e);
    });
}
